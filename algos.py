""" Модуль, содержащий различные алгоритмы и их версии
author: watooh@internet.ru
"""

import random
from typing import List, Tuple, Any


def erato_sieve(num: int) -> List[int]:
    """ Классическое решето Эратосфена
    :param n: Верхняя граница поиска простых чисел
    :return: Список простых чисел
    """
    if num < 1:
        return []
    prime = [True, ] * (num+1)
    prime[0] = prime[1] = False
    for i in range(2, num):
        if prime[i] and i*i <= num:
            for j in range(i*i, num, i):
                prime[j] = False
    return [i for i, v in enumerate(prime[:num]) if v]


def _linear_erato_sieve(num: int) -> Tuple[List, List]:
    """ Решето Эратосфена за линейное время
    :param n: Верхняя граница поиска простых чисел
    :return: Список наименьших простых делителей и список простых чисел
    """
    lesser_primals = [0, ] * (num + 1)
    primals = []

    for i in range(2, num):
        if not lesser_primals[i]:
            lesser_primals[i] = i
            primals.append(i)
        j = 0
        while j < len(primals) and primals[j] <= lesser_primals[i]:
            lp_ = i * primals[j]
            if lp_ < num:
                lesser_primals[lp_] = primals[j]
            else:
                break
            j += 1
    return primals, lesser_primals


def linear_erato_sieve(num: int) -> List:
    """ Решето Эратосфена за линейное время
    :param n: Верхняя граница поиска простых чисел
    :return: Список простых чисел
    """
    return _linear_erato_sieve(num)[0]


def factorization(num: int) -> List:
    """Линейное разложение на простые множители
    Используется список наименьших простых делителей числа
    :param n: Исходное число
    :return: Список простых множителей
    """
    result = []
    sieve = _linear_erato_sieve(num+1)[1]

    cur_num = num
    while cur_num != 1:
        result.append(sieve[cur_num])
        cur_num = int(cur_num / sieve[cur_num])

    return result


def binpow(base: Any, power: int) -> Any:
    """ Бинарное возведение числа в целую степень
    :param base: Число
    :param power: Степень
    :return: Число a в степени n
    """
    res = None
    while power:
        if power % 2 == 1:
            res = res * base if res else base
        base *= base
        power = power >> 1
    return res or 1


def euler(num: int) -> int:
    """ Функция Эйлера
    :param num: Аргумент функции Эйлера
    :return: Количество взаимно простых чисел с n на промежутке 1..n-1
    """
    res = num

    for i in factorization(num):
        if num % i == 0:
            while num % i == 0 and num != i:
                num /= i
            res -= res/i

    return int(res)


def gcd(arg_1: int, arg_2: int) -> int:
    """ Алгортитм Евклида
    :param arg_1: аргумент 1
    :param arg_2: аргумент 2
    :return: Наибольший общий делитель arg_1 и arg_1
    """
    if arg_2 == 0:
        return arg_1
    return gcd(arg_1, arg_1 % arg_2)


# некорректно
def gcd_ext(arg_1: int, arg_2: int) -> Tuple[int, float, float]:
    """ Расширенный алгоритм Евклида
    :param arg_1: аргумент 1
    :param arg_1: аргумент 2
    :return: НОД(arg_1,arg_1), mul1, mul2 такие, что mul1*arg_1+mul2*arg_1=НОД(arg_1,arg_1)
    """
    if arg_1 == 0:
        mul1 = 0
        mul2 = 1
        return arg_2, mul1, mul2
    val, mul1, mul2 = gcd_ext(arg_2 % arg_1, arg_1)
    mul1 = mul2 - (arg_2 / arg_1) * mul1
    mul2 = mul1
    return val, mul1, mul2


class _matrix():
    """ Вспомогательный класс матриц
    """
    def __init__(self, matrix):
        self.matrix = matrix

    @property
    def height(self) -> int:
        """ Количество строк матрицы
        :return:
        """
        return len(self.matrix)

    @property
    def width(self) -> int:
        """ Длина строк матрицы
        :return:
        """
        return len(self.matrix[0])

    def __mul__(self, other):
        if self.width != other.height:
            raise Exception('Cannot multiply those matrixes')

        res = _matrix([[None, ]*self.height for _ in range(self.width)])
        for i, _ in enumerate(res):
            for j in range(len(res[i])):
                res[i, j] = sum(self[i,r]*other[r,j] for r in range(self.width))

        return res

    def __getitem__(self, item):
        if isinstance(item, tuple):
            i, j = item
            return self.matrix[i][j]
        return self.matrix[item]

    def __setitem__(self, key, value):
        if isinstance(key, tuple):
            i, j = key
            self.matrix[i][j] = value
            return
        self.matrix[key] = value

    def __len__(self):
        return len(self.matrix)


def fib(num: int) -> int:
    """ Алгоритм вычисления n числа Фибоначчи
    Использует свойство матрицы  (0, 1)
                                 (1, 1)
    Если её возвести в степень n - 0,0 элемент матрицы будет искомым числом
    :param num: Номер числа Фибоначчи
    :return: Число Фибоначчи
    """
    matrix = _matrix([(0, 1),
                      (1, 1)])
    return binpow(matrix, num + 1)[0, 0]


def fib_irr(num: int) -> int:
    """ Алгоритм поиска n числа Фибоначчи
    Использует указанную ниже формулу
    Формула даёт неточность при n >= ~ 80
    :param num: Номер числа Фибоначчи
    :return: Число Фибоначчи
    """
    sq5 = 5**(1/2)
    return int((((1+sq5)/2)**num - ((1-sq5)/2)**num)/sq5)


def gray(num: int) -> int:
    """ Перевод в код Грея
    :param n: исходное число
    :return: представление Грея
    """
    return num ^ (num >> 1)


class Tree:
    """ Дерево Куча
    """
    def __init__(self, value, left=None, right=None):
        self.value = value
        self.left = left
        self.right = right

    def __add__(self, other):
        if not isinstance(other, Tree):
            other = Tree(other)
        return self.__add_tree__(other)

    def __add_tree__(self, other):
        """Алгоритм слияния двух деревьев
        """
        tree_1 = self
        tree_2 = other
        if tree_1.value < tree_2.value:
            tree_1, tree_2 = tree_2, tree_1
        if tree_1.left:
            tree_1.left, tree_1.right = tree_1.right, tree_1.left
        if not tree_1.left:
            tree_1.left = tree_2
        else:
            tree_1.left = tree_1.left.__add_tree__(tree_2)
        return tree_1

    def str_recursive(self, offset) -> str:
        """ Рекурсивный вывод дерева
        :param offset: Отступ уровней дерева
        """
        res = str(self.value)
        if self.left:
            left = f'-L->{self.left.str_recursive(offset+4)}'
            res += '\n' + ' '*offset + left
        if self.right:
            right = f'-R->{self.right.str_recursive(offset+4)}'
            res += '\n' + ' '*offset + right
        return res

    def __str__(self):
        return self.str_recursive(2)


def merge_sort(list_: List) -> List:
    """ Сортировка слиянием
    :param list_: исходный список
    :return: отсортированный список
    """
    len_ = len(list_)
    if len_ == 1:
        return list_
    if len_ == 2:
        if list_[0] > list_[1]:
            list_ = [list_[1], list_[0]]
        return list_
    equator = len_ // 2
    left = merge_sort(list_[:equator])
    right = merge_sort(list_[equator:])
    res = []
    for _ in range(len_):
        if not left:
            left = right
        if left[-1] < right[-1]:
            left, right = right, left

        res.append(left.pop())

    return list(reversed(res))


def quick_sort(list_: List) -> List:
    """ Рекурсивный алгоритм быстрой сортировки
    :param list_: исходный список
    :return: отсортированный список
    """
    len_ = len(list_)
    if len_ < 2:
        return list_
    base = random.choice(list_)

    left_part = [el for el in list_ if el < base]
    right_part = [el for el in list_ if el >= base]

    return quick_sort(left_part)+quick_sort(right_part)


def int_to_roman(num: int) -> str:
    """ Алгоритм перевода целых чисел в римские
    :param num: исходное целое число
    :return: римское представление
    """
    symbol_values = {
        'I': 1,
        'V': 5,
        'X': 10,
        'L': 50,
        'C': 100,
        'D': 500,
        'M': 1000
    }

    good_replacement = {
        'IIII': 'IV',
        'XXXX': 'XL',
        'CCCC': 'CD',
        'VIIII': 'IX',
        'LXXXX': 'XC',
        'DCCCC': 'CM',
    }

    symbol_cnts = {}

    for sym, val in reversed(symbol_values.items()):
        num, cnt = num % val, num // val
        symbol_cnts[sym] = cnt

    res = []

    for sym, cnt in symbol_cnts.items():
        to_add = sym * cnt
        last = res.pop() if res else ''
        to_add = good_replacement.get(last + to_add,
                 last + good_replacement.get(to_add, to_add))
        res += to_add

    return ''.join(res)
