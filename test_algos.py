""" Модуль для тестирования algos.py
"""
import random
import pytest
from algos import erato_sieve, linear_erato_sieve, factorization, binpow

# pylint: disable=missing-function-docstring


def test_erato_sieve():
    assert not erato_sieve(0)
    assert not erato_sieve(1)
    assert not erato_sieve(2)
    assert len(erato_sieve(30)) == 10


def test_linear_erato_sieve():
    assert not linear_erato_sieve(0)
    assert not linear_erato_sieve(1)
    assert not linear_erato_sieve(2)
    assert len(linear_erato_sieve(30)) == 10


def test_factorization():
    with pytest.raises(ZeroDivisionError):
        factorization(0)
    assert not factorization(1)
    assert factorization(50) == [2, 5, 5]


def test_binpow():
    for _ in range(10):
        base = random.randint(0, 100)
        power = random.randint(0, 100)

        assert base**power == binpow(base, power)
